#Written By Ricardo
#CIS 422
#06/04/2019
from tkinter import filedialog
from tkinter import *
from Campaign import *
import sqlite3 

class CampaignSelect(Frame):

	def __init__(self, master=None):
		Frame.__init__(self, master)
		
		self.pack()
		self.configure(background='black')	
		self.conn = sqlite3.connect('toolkit.sqlite')
		cur = self.conn.cursor()
		"""
		creates all tables first time ran
		iff the tables do not currently exist in the database
		"""
		cur.execute('CREATE TABLE IF NOT EXISTS campaign (name TEXT, pathtocampaign TEXT)')
		cur.execute('CREATE TABLE IF NOT EXISTS character (name TEXT, charactername TEXT, health INT, race TEXT, gender TEXT, height TEXT, faction TEXT, pathtophoto TEXT)')
		cur.execute('CREATE TABLE IF NOT EXISTS spells (name TEXT, count INT, pathtofile TEXT, active INT)')
		cur.execute('CREATE TABLE IF NOT EXISTS stats (name TEXT, count INT, pathtofile TEXT, active INT)')
		cur.execute('CREATE TABLE IF NOT EXISTS inventory (name TEXT, pathtofile TEXT)')
		#check the entire database to see all the campaign names stored in campaign tables
		cur.execute("SELECT name FROM campaign")
		allcampaigns = cur.fetchall()
		#default name to be displayed is Add Campaign for a button
		self.one = "Add Campaign"
		self.two = "Add Campaign"
		self.three = "Add Campaign"
		self.four = "Add Campaign"
		self.five = "Add Campaign"
		#adds them to a list to be accessed
		campaignnames = []
		cur.close()

		#updates the name of buttons if campaigns exist in the database
		for campaign in allcampaigns:
			if(campaign[0] != "campaign"):
				campaignnames.append(campaign[0].replace("_", " "))
		name_count = len(campaignnames)
		if(name_count > 0):
			self.one = campaignnames[0]
		if(name_count > 1):
			self.two = campaignnames[1]
		if(name_count > 2):
			self.three = campaignnames[2]
		if(name_count > 3):
			self.four = campaignnames[3]
		if(name_count > 4):
			self.five = campaignnames[4]
		self.createWidgets()

		master.bind('1', self.slot1['command'])
		master.bind('2', self.slot2['command'])
		master.bind('3', self.slot3['command'])
		master.bind('4', self.slot4['command'])
		master.bind('5', self.slot4['command'])
		master.bind('q', self.QUIT['command'])
		master.protocol("WM_DELETE_WINDOW", self.quit)

	def Campaign(self, name, event=None) :
		'''
		Function that gets called when you select a campaign. If adding campaign go to Campaign window.  If clicking on preexisting course go to the Campaign with no name passed. 
		'''
		if (name=="Add Campaign"):
			self.createCampaign("")
		else:
			self.createCampaign(name)
		
	def createCampaign(self, name):
		'''
		Builds the Campaign Frame
		'''
		# Hide this Frame
		self.master.withdraw()
		# Build Edit Roster
		root = Tk()
		if(name != ""):
			root.title("Welcome Back to " + name)
		else:
			root.title("Create Campaign")
		#auto adjust initial frame size to fit.
		root.geometry("")
		app = Campaign(master=root, name=name)
		app.mainloop()
		root.destroy()
		self.master.deiconify()	

	def createWidgets(self):
                '''
                Creates and specifies the locations of buttons
                '''
                self.header = Label(self, text = "Select Campaign:", background = 'black',foreground= "white" ,font = ("Comic Sans MS",24))

                # Campaign 1
                self.slot1 = Button(self, text = "" + self.one , highlightbackground='#000000', font = ("Comic Sans MS",16), command = lambda: self.Campaign(self.one))
                # Campaign 2
                self.slot2 = Button(self, text = "" + self.two, highlightbackground='#000000',font = ("Comic Sans MS",16), command = lambda: self.Campaign(self.two))
                # Campaign 3
                self.slot3 = Button(self, text ="" + self.three, highlightbackground='#000000',font = ("Comic Sans MS",16), command = lambda: self.Campaign(self.three))
                #Campaign 4
                self.slot4 = Button(self, text ="" + self.four, highlightbackground='#000000',font = ("Comic Sans MS",16), command = lambda: self.Campaign(self.four))
				#Campaign 5
                self.slot5 = Button(self, text = "" + self.five, highlightbackground='#000000',font = ("Comic Sans MS",16), command = lambda: self.Campaign(self.five))
				# Quit button
                self.QUIT = Button(self, text = "QUIT <q>",bg = 'black', fg ="white"  ,highlightbackground='#000000',font = ("Herculanum",16), command = self.quit)
                self.QUIT["fg"]   = "red"
                self.QUIT["font"] = "Herculanum"
				# Organize elements
                self.header.pack({"side": "top"})
                self.slot1.pack()
                self.slot2.pack({"side": "top"})
                self.slot3.pack({"side": "top"})
                self.slot4.pack({"side" : "top"})
                self.slot5.pack({"side" : "top"})

                """
                Quit Button
                """
                self.QUIT.pack({"side": "top"})
           
