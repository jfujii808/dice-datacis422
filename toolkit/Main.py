#Written By Ricardo
#CIS 422
#06/04/2019
from CampaignSelect import *
"""
Main Frame that opens the toolkit
Welcome to the Toolkit!
Please check Programmer's Documentation for summary notes of modules and functions
Please enjoy the application! I'm very proud of the outcome.
"""
def main():
	root = Tk()
	root.title("TABLETOP TOOLKIT")
	root.geometry("")
	rostHome = CampaignSelect(master=root)
	rostHome.mainloop()
	root.destroy()
    

if __name__=="__main__":
    main()
