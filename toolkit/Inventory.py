#Written By Ricardo
#CIS 422
#06/04/2019
import sqlite3 
from tkinter import *
from tkinter import ttk
import tkinter as tk
from tkinter import messagebox
import types
import csv

"""
This View displays information the user will ideally use to keep track of all currently held inventory their character possesses
"""
class Inventory(Frame):
    def __init__(self, name = None, master=None):
            '''
            Constructor
            Only sets up two columns since more information about an item doesn't seem necessary
            '''
            Frame.__init__(self, master)
            self.name = name
            self.tree=ttk.Treeview(master, show = "headings")
            self.conn = sqlite3.connect('toolkit.sqlite')
            cur = self.conn.cursor()
            cur.execute("SELECT * FROM Inventory WHERE name=?", (self.name,))
            rows = cur.fetchall()
            cur.close()

            self.pathtofile = rows[0][1]
            self.count = rows[0][1]
            self.tree["columns"]=("one","two")
            self.tree.heading("#1", text = "Inventory")
            self.tree.heading("#2", text = "Description")
            self.tree.column("#1", minwidth=0, width=100)
            self.tree.column("#2", minwidth=0, width=300)
            self.pack()
            self.createWidgets()
            self.tree.pack(side="top", fill="both", expand=True)
            master.bind('q', self.BACK['command'])
            master.protocol("WM_DELETE_WINDOW", self.quit) 
            self.View()

    def View(self): #same view function as Spells.py
    	self.fixup()
    	self.cleanup()
    	file = open(self.pathtofile, "r")
    	self.userstats = []
    	with file:
    		reader = csv.reader(file)
    		for row in reader:
    			self.tree.insert("", tk.END, values=row)
    			self.userstats.append(row[0])
    
    def createWidgets(self): #similar functionality as every other createWidgets
    	self.deleteItemB = Button(self, text = "Delete Item", command = lambda: self.deleteItem())
    	self.addItemB = Button(self, text = "Add Item", command = lambda: self.addItem())
    	self.BACK = Button(self, text = "BACK <q>", command = self.quit)
    	self.BACK["fg"]   = "red"
    	self.BACK.pack({"anchor":"e"})
    	self.addItemB.pack({"side" : "top"})
    	self.deleteItemB.pack({"side" : "top"})

    	"""
    	Following functions can all be found in siimilar forms in the Spells.py and Stats.py files.
    	"""
    def addItem(self):
    	names = []
    	linect = 0
    	file = open(self.pathtofile)
    	for line in file:
    		linect+=1
    	newinventory_name = simpledialog.askstring("Item", "Insert Name of Item")
    	names.append(str(linect)+". "+newinventory_name)
    	newinventory_description = simpledialog.askstring("Description", "Insert Description")
    	names.append(newinventory_description)
    	file = open(self.pathtofile, "a")
    	with file:
    		writer = csv.writer(file)
    		writer.writerow(names)
    	self.View()

    def deleteItem(self):
        deleteline = simpledialog.askstring("Delete", "Enter Line Number")
        if(self.intcheck(deleteline)):
            choice = int(float(deleteline))
            #print(choice)
            with open(self.pathtofile, "r") as infile:
                lines = infile.readlines()
            #print(lines)
            with open(self.pathtofile, "w") as outfile:
                for pos, line in enumerate(lines):
                    if (pos+1) != choice:
                        line[2:]
                        outfile.write(line)
            self.View()

        else:
            messagebox.showwarning("OOPS!", "That there is not an INTEGER!")

    def intcheck(self, check):
    	try:
    		test = int(float(check))
    		if(isinstance(test, int)):
    			return True
    	except:
    		return False

    def fixup(self):
    	with open(self.pathtofile, "r") as infile:
    		lines = infile.readlines()
    	with open(self.pathtofile, "w") as outfile:
    		for pos, line in enumerate(lines):
    				outfile.write(str(pos+1)+". "+line[3:])
    def cleanup(self):
    	for row in self.tree.get_children():
    		self.tree.delete(row)