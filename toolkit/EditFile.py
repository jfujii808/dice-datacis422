#Written By Ricardo
#CIS 422
#06/04/2019
from tkinter import filedialog
from tkinter import *
import re
from tkinter.filedialog import askdirectory
import os

class Window(Frame):
    def __init__(self, master=None, path = None, name = None):
        Frame.__init__(self, master)
        self.master = master

        self.master.title("{} Campaign Notes".format(name))
        self.pack(fill=BOTH, expand=1)

        top = self.master
        menu = Menu(top)
        top.config(menu=menu)
        self.file_menu = Menu(menu)
        self.path = path
        menu.add_cascade(label="File", menu=self.file_menu)
        master.protocol("WM_DELETE_WINDOW", self.quit)
        self.file_menu.add_command(label="Save", command=self.save_file_function)
        self.file_menu.add_separator()

        self.text = Text(top, height=200, width=100) #Use Text widget insted of Listbox
        self.text.pack(side=LEFT, expand=True)

        #adds a scroll bar that stretches the y-axis
        self.scrollbar = Scrollbar(top, orient="vertical")
        self.scrollbar.config(command=self.text.yview)
        self.scrollbar.pack(side=RIGHT, fill=Y, expand=True)

        self.text.config(yscrollcommand=self.scrollbar.set) 
        #calls the open file function on initialization so the user does not have to find the text file everytime, or at all
        self.open_file_function()


    def open_file_function(self): #pass in path to file
        if(os.path.exists(self.path)): #if file exists simply write to the tree
            file = open(self.path, "r")
            for i in file:
                self.text.insert(END, i)
        else:
            file = open(self.path, "w")#if not then we simply create the file. Which will get written to at a later point by the save file function

    def save_file_function(self, path = None):
        self.open_file = open(self.path, "w+")
        if self.open_file != None:
        # slice off the last character from get, as an extra return is added
            data = self.text.get('1.0', 'end-1c')
            self.open_file.write(data)
            self.open_file.close()

    #destroys current view. returns control to the previous/calling root
    def exit_function(self):   
        self.root.destroy()
