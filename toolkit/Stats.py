#Written By Ricardo
#CIS 422
#06/04/2019
import sqlite3 
from tkinter import *
from tkinter import ttk
import tkinter as tk
from tkinter import messagebox
import types
import csv
"""
This script will interface with the database as well as an external .csv file to keep track of all stats a character currently has.
Displaying the stats basic information such as name, and value.
Serves more as a reference sheet.
"""
class Stats(Frame):
    def __init__(self, name = None, master=None):
            '''
            Constructor
            Used a Tree struture for displaying information for its ability to display information in column/table like formats
            Also proved to be useful using .csv files for this type of implementation
            '''
            Frame.__init__(self, master)
            self.name = name
            self.tree=ttk.Treeview(master, show = "headings")
            self.conn = sqlite3.connect('toolkit.sqlite')
            cur = self.conn.cursor()
            cur.execute("SELECT * FROM stats WHERE name=?", (self.name,))
            rows = cur.fetchall()
            cur.close()
            #grabs the 3rd column's value of the returned row of this table. Which happens to be the path to the inventory file
            self.pathtofile = rows[0][2]
            self.tree["columns"]=("one","two")
            self.tree.heading("#1", text = "Stat")
            self.tree.heading("#2", text = "Current Value")
            self.tree.column("#1", minwidth=0, width=100)
            self.tree.column("#2", minwidth=0, width=300)
            self.pack()
            self.createWidgets()
            self.tree.pack(side="top", fill="both", expand=True)
            master.bind('q', self.BACK['command'])
            master.protocol("WM_DELETE_WINDOW", self.quit) 
            self.View() #first call to View to load any previously input file information into the text widget 

    def View(self):
    	self.fixup()
    	self.cleanup()
    	file = open(self.pathtofile, "r")
    	self.userstats = []
    	with file:
    		reader = csv.reader(file)
    		for row in reader:
    			self.tree.insert("", tk.END, values=row)
    			self.userstats.append(row[0])
    
    def createWidgets(self):
    	self.editStatB = Button(self, text = "Edit Stat", command = lambda: self.editStat())
    	self.BACK = Button(self, text = "BACK <q>", command = self.quit)
    	self.BACK["fg"]   = "red"

    	self.BACK.pack({"anchor":"e"})
    	self.editStatB.pack({"side" : "top"})

    """
    Edit stat allows the user to edit a prexisting stat's value.
    Does not allow for adding or deleting of stats since they tend to be more static quantities
    """
    def editStat(self):
    	deleteline = simpledialog.askstring("Edit", "Enter Line Number")
    	if(self.intcheck(deleteline)):
    		choice = int(float(deleteline))
    		#print(choice)
    		newvalue = simpledialog.askstring("Update", "Enter New Value")
    		with open(self.pathtofile, "r") as infile:
    			lines = infile.readlines()
    		with open(self.pathtofile, "w") as outfile:
    			for pos, line in enumerate(lines):
    				if pos != (choice-1):
    					outfile.write(str(pos)+". "+line[3:])
    				else:
    					outfile.write(self.userstats[pos]+","+newvalue+"\n")
    		self.View() #calls view to add show the newly updated information based off user input

    	else:
    		messagebox.showwarning("OOPS!", "That there is not an INTEGER!")
    #simple int check, please advise Campaign.py for further details
    def intcheck(self, check):
    	try:
    		test = int(float(check))
    		if(isinstance(test, int)):
    			return True
    	except:
    		return False
    """
    this functions truncates each line in the file 3 characters to delete the first stored row number,
    period, and the space character. All in order to update the new row number once an edit or change has been made.
    If this step is not taken the strings will indefinetly grow after each iteration.
    """
    def fixup(self):
    	with open(self.pathtofile, "r") as infile:
    		lines = infile.readlines()
    	with open(self.pathtofile, "w") as outfile:
    		for pos, line in enumerate(lines):
    				outfile.write(str(pos+1)+". "+line[3:])
    """
    cleans the tree of information. called before writing to the tree at any time
    """
    def cleanup(self):
    	for row in self.tree.get_children():
    		self.tree.delete(row)