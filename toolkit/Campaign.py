#Written By Ricardo
#contribution from Cameron Mcewon
#CIS 422
#06/04/2019
from tkinter import *
from tkinter.filedialog import askdirectory
from tkinter import simpledialog
from tkinter import messagebox
from EditFile import *
from Spells import *
from Stats import*
from Inventory import *
import os, types
import csv

class Campaign(Frame):
        """
        The campaign window shows the campaign, stats, spells,
        inventory, character info, and allows the campaign
        notes to be edited.

        """
        def __init__(self, name = None, master=None):
                '''
                Constructor
                '''
                self.master=master
                self.conn = sqlite3.connect('toolkit.sqlite')
                self.name = name
                self.charactername = ""
                self.health = 0 
                self.gender = ""
                self.height = ""
                self.race = ""
                self.faction = ""
                self.pathtophoto = ""
                lego = os.path.join(os.path.dirname(os.path.abspath(__file__)), "images/d20red.gif")
                if name != "":
                        self.load()
                else:
                        self.image2 = PhotoImage(master=master,file = lego)             
                Frame.__init__(self, master)
                self.pack()
                self.createWidgets()
                # Assign keybinds
                master.bind('1', self.characterB['command'])
                master.bind('2', self.campaignB['command'])
                master.bind('3', self.statsB['command'])
                master.bind('4', self.inventoryB['command'])
                master.bind('5', self.spellsB['command'])
                master.bind('q', self.BACK['command']) 
                master.protocol("WM_DELETE_WINDOW", self.quit)

        def load(self):
                '''
                If the table exists in the database all of the user's information is loaded into this instance
                Naming convention is pretty straight forward 
                '''
                cur = self.conn.cursor()
                cur.execute('SELECT * FROM character WHERE name=?;', [self.name])
                row = cur.fetchall()
                self.charactername = row[0][1]
                self.health = row[0][2]
                self.race = row[0][3]
                self.gender = row[0][4]
                self.height = row[0][5]
                self.faction = row[0][6]
                self.pathtophoto = row[0][7]
                self.image2 = PhotoImage(master=self.master, file = self.pathtophoto) #stores the image version pointed to from the path in the db

        def createWidgets(self):
                '''
                Creates the buttons for this Frame and organizes them
                '''
                if self.name == "":
                        header = "Create New"
                else:
                        header = "{}".format(self.name)
                # Create Buttons
                self.header = Label(self, text = header, font = ("Courier", 16))
                self.characterB = Button(self, text = "Edit Character", background = 'black',foreground= "white",command = lambda: self.createCharacter())
                self.campaignB = Button(self, text = "Edit Campaign Notes", command = lambda: self.edit_campaign())
                self.healthB = Button(self, text = "HEALTH", command = lambda: self.updateHealth())
                self.statsB = Button(self, text = "Stats", command = lambda: self.createStats())
                self.inventoryB = Button(self, text= "Inventory", command = lambda: self.createInventory())
                self.spellsB = Button(self, text= "Spells", command = lambda: self.createSpells())
                self.deleteB = Button(self, text= "Delete", command = lambda: self.deleteCampaign())
                self.bio = Text(self,borderwidth=2,relief="groove", height = 25, width = 20)
                self.inv = Text(self,borderwidth=2,relief="groove", height = 25, width = 20)      
                self.profpic = Label(self)
                self.profpic.configure(image=self.image2)
                self.inv_label= Label(self, text = "Inventory", font = ("Courier", 16))
                self.bio_label= Label(self, text = "BIO", font = ("Courier", 16))

                # Quit button
                self.BACK = Button(self, text = "BACK <q>", command = self.quit)
                self.BACK["fg"]   = "red"
                
                #function cleans the text widgets in campaign and populates them based on files
                self.cleanup() 

                # Organize the elements using grid layout
                self.header.grid(row=0,columnspan=3)
                self.characterB.grid(row= 3, column = 0, padx = (300,0), pady =(65, 0))
                self.campaignB.grid(row= 3, column = 0, padx = (0,300), pady =(5, 0))
                self.healthB.grid(row= 2, column = 0, padx = (0,0), pady =(0, 175))
                self.deleteB.grid(row= 2, column = 0, padx = (0,0), pady =(0, 410))
                self.profpic.grid(row = 1, column = 0, padx = (0,0), pady =(0, 0))
                self.statsB.grid(row= 0, column = 0, padx = (300,0), pady =(0, 0))
                self.inventoryB.grid(row= 3, column = 0, padx = (300,0), pady =(0, 0))
                self.spellsB.grid(row= 3, column = 0, padx = (0,300), pady =(65, 0))
                self.BACK.grid(row= 0, column = 0, padx = (0,300), pady =(0, 0))
                self.bio.grid(row = 2, column = 0, padx = (0,300), pady =(0, 0))
                self.inv.grid(row = 2, column = 0, padx = (300,0), pady =(0, 0))
                self.inv_label.grid(row = 2, column = 0, padx = (300,0), pady =(0, 430))
                self.bio_label.grid(row = 2, column = 0, padx = (0,300), pady =(0, 430))

        def create_edit(self):
                """
                Gets called from Edit Campaign. 
                Actually calls the Edit File Frame into view
                Supplying it with the given path
                """
                if self.name != "":
                        self.master.withdraw()
                        root = Tk()
                        root.title("{} Campaign".format(self.name))
                        cur = self.conn.cursor()
                        cur.execute('SELECT pathtocampaign FROM campaign WHERE name=?;', [self.name])
                        row = cur.fetchall()
                        self.path = row[0][0]
                        #print(self.path)

                        #Center frame to window.
                        root.geometry("")

                        app = Window(master=root, path=self.path, name=self.name)
                        app.mainloop()
                        root.destroy()
                        self.master.deiconify()
                else:
                        messagebox.showwarning("OOPS", "You need a name first")

        def edit_campaign(self, event=None): #find file path and send the path to create edit which opens the text editor
                if self.name == None:
                        self.name = simpledialog.askstring("Enter Campaign Name", "Make it unique!")
                # Get the path to the campaign notes
                        path = os.cwd()
                        self.path = path+"/notes/"+self.name+".txt"
                        file = open(self.path, "w")
                        file.close()
                        cur = self.conn.cursor()
                        #database check to see if a file exists for this campaign
                        cur.execute('SELECT pathtocampaign FROM campaign WHERE name=?', (self.name,))
                        check = cur.fetchall()
                        if len(check) == 0:
                                cur.execute('INSERT INTO campaign VALUES (?, ?)', (self.name, self.path))
                        self.conn.commit()
                        cur.close()
                self.create_edit()

        def deleteCampaign(self, event=None) :
                '''
                Remove all instances of this campaign from each database.
                inventory
                spells
                stats
                character
                campaign
                '''
                if (self.name == None):
                        messagebox.showwarning("Warning", "There is no name to remove!")
                else:
                        pos = messagebox.askokcancel("Question", "Are you sure you want to remove {}?".format(self.name))
                        if (pos):
                                cur = self.conn.cursor()
                                
                                #Removes campaign from each table in the db
                                cur.execute('SELECT * FROM stats WHERE name=?', (self.name,))
                                pre_pho = cur.fetchall()
                                if len(pre_pho) != 0:
                                        cur.execute('DELETE FROM stats WHERE name=?', (self.name,))
                                        self.conn.commit()
                                cur.execute('SELECT * FROM spells WHERE name=?', (self.name,))
                                pre_pho = cur.fetchall()
                                if len(pre_pho) != 0:
                                        cur.execute('DELETE FROM spells WHERE name=?', (self.name,))
                                        self.conn.commit()
                                cur.execute('SELECT * FROM inventory WHERE name=?', (self.name,))
                                pre_pho = cur.fetchall()
                                if len(pre_pho) != 0:
                                        cur.execute('DELETE FROM inventory WHERE name=?', (self.name,))
                                        self.conn.commit()
                                cur.execute('SELECT * FROM character WHERE name=?', (self.name,))
                                pre_pho = cur.fetchall()
                                if len(pre_pho) != 0:
                                        cur.execute('DELETE FROM character WHERE name=?', (self.name,))
                                        self.conn.commit()
                                cur.execute('SELECT * FROM campaign WHERE name=?', (self.name,))
                                pre_pho = cur.fetchall()
                                if len(pre_pho) != 0:
                                        cur.execute('DELETE FROM campaign WHERE name=?', (self.name,))
                                        self.conn.commit()
                                path = os.getcwd()
                                """
                                Each delete only removes instances in the database
                                We need to also cleanup all the files we created
                                Following code deletes each of those files
                                """
                                pathtocampaign = path+"/notes/"+self.name+".txt"
                                pathtoinventory = path+"/inventory/"+self.name+"_Inventory.csv"
                                pathtospells = path+"/spells/"+self.name+"_Spells.csv"
                                pathtostats = path+"/stats/"+self.name+"_Stats.csv"
                                pathtobio = path+"/bio/"+self.name+"_Bio.csv"
                                if os.path.exists(pathtocampaign):
                                        os.remove(pathtocampaign)
                                if os.path.exists(pathtoinventory):
                                        os.remove(pathtoinventory)
                                if os.path.exists(pathtospells):
                                        os.remove(pathtospells)
                                if os.path.exists(pathtostats):
                                        os.remove(pathtostats)
                                if os.path.exists(pathtobio):
                                        os.remove(pathtobio)
                                
                                cur.close()
                                messagebox.showinfo("Information", "Please restart Toolkit to see changes.")
                                messagebox.showinfo("THANK YOU", "THANK YOU FOR USING MY TOOLKIT.")

        def updateHealth(self): #doesn't need to create a new window
                '''
                Prompts a new value for the Health Field
                '''
                #Makes sure that the file exists
                if(self.name != "" and self.name != None):
                        new_health_str = simpledialog.askstring("Update Health", "ENTER CURRENT HP")
                        #calls intcheck to see if the response can actually be converted to an int
                        if(self.intcheck(new_health_str)):
                                self.health = int(float(new_health_str))
                                cur = self.conn.cursor()
                                cur.execute('UPDATE character SET health = (?) WHERE name = (?)', (self.health, self.name.replace(" ", "_")),)
                                self.conn.commit()
                                path = os.getcwd()
                                biopath = path+"/bio/"+self.name+"_Bio.csv"
                                if os.path.exists(biopath):
                                    os.remove(biopath)
                                with open(biopath, "a") as f:
                                    f.write("Name: "+self.charactername+",as\n"+"Health: "+str(self.health)+",as\n"+"Race: "+self.race+",as\n"+"GenIden: "+self.gender+",as\n"+"HEIGHT: "+self.height+",as\n"+"FACTION: "+self.faction+",as\n")
                                cur.close()
                                self.cleanup()


                        else:
                                messagebox.showwarning("OOPS!", "That there is not an INTEGER!")
                else:
                        messagebox.showwarning("AHHH!", "You're a nameless Wizard! Return when you have a name and campaign")

        def createCharacter(self):
                '''
                Does not create a new frame but makes one call to the database to add in all character info
                Makes another database call to create the campaign info
                '''
                self.check = 0 #check is used to monitor progress throughout registration process
                if(self.charactername == "" or self.charactername == None): #quick check to make sure that this campaign is empty
                        messagebox._show("HELLO", "Looks like you're a nameless warrior")
                        self.charactername = simpledialog.askstring("Character Name", "Name your CHAMPION")
                        if(self.charactername != "" and self.charactername != None): #validation check
                                self.check += 1

                if(self.name == "" or self.name == None and check >= 1):
                        self.name = simpledialog.askstring("Campaign Name", "Please enter the name of your campaign")
                        if(self.name != "" and self.name != None):
                                self.check += 1

                if(self.health == 0 or self.health == None and check >= 2):
                        health = simpledialog.askstring("HEALTH","Please enter your maximum HP *e.g. 500*")
                        if(self.intcheck(health)):
                                self.health = health

                        else:
                                messagebox.showwarning("OOPS!", "That there is not an INTEGER!")
                        if(self.health != "" and self.health != None):
                                self.check += 1

                if(self.race == "" or self.race == None and check >= 3):
                        self.race = simpledialog.askstring("","RACE")
                        if(self.race != "" and self.race != None):
                                self.check += 1
                if(self.gender == "" or self.gender == None and check >= 4):
                        self.gender = simpledialog.askstring("","GENDER IDENTITY")
                        if(self.gender != "" and self.gender != None):
                                self.check += 1
                if(self.height == "" or self.height == None and check >= 5):
                        self.height = simpledialog.askstring("","HEIGHT *e.g. 6'11*")
                        if(self.height != "" and self.height != None):
                                self.check += 1
                if(self.faction == "" or self.faction == None and check >= 6):
                        self.faction = simpledialog.askstring("","FACTION NAME")
                        if(self.faction != "" and self.faction != None):
                                self.check += 1
                messagebox._show("PHOTO", "Select a Photo as your portrait!")
                if(self.pathtophoto == "" or self.pathtophoto == None and check >= 7):
                        picpath = os.path.abspath(os.path.join(os.getcwd(), os.pardir)) #this basically grabs the path to the parent directory
                        picpath = picpath+"/src/diceassets" #this completes the path to point to where our dice assets are
                        self.pathtophoto = filedialog.askopenfilename(initialdir = picpath) #sets the previous path as the initial directory when asking for a file
                        if(self.pathtophoto != "" and self.pathtophoto != None):
                                self.check += 1

                cur = self.conn.cursor()
                if(self.check == 8): #makes sure we are all done
                        cur.execute('INSERT INTO character VALUES (?, ?, ?, ?, ?, ?, ?, ?)', (self.name, self.charactername, self.health, self.race, self.gender, self.height, self.faction, self.pathtophoto)) #if we're done data gets committed to the database
                        path = os.getcwd()
                        self.path = path+"/notes/"+self.name+".txt"
                        biopath = path+"/bio/"+self.name+"_Bio.csv"
                        with open(biopath, "a") as f:
                                f.write("Name: "+self.charactername+",as\n"+"Health: "+str(self.health)+",as\n"+"Race: "+self.race+",as\n"+"GenIden: "+self.gender+",as\n"+"HEIGHT: "+self.height+",as\n"+"FACTION: "+self.faction+",as\n")
                        cur.execute('INSERT INTO campaign VALUES (?, ?)', (self.name, self.path))
                        self.conn.commit()
                        self.cleanup()
                        cur.close()
                else:
                        messagebox.showwarning("OOPS!", "Character was not saved. Go back and complete required information")

        def createInventory(self):
                '''
                Creates Inventory Frame
                Go to Inventory.py for more information
                Or read programmer's documentation for an overview of the information
                '''
                if(self.name != None and self.charactername != ""):
                        cur = self.conn.cursor()
                        cur.execute("SELECT * FROM inventory WHERE name=?", (self.name,)) #database check if this campaign owns a file already
                        rows = cur.fetchall()
                        cur.close()

                        if rows: #if row in database exists, then we open the specified 
                                # Hide this Frame
                                self.master.withdraw()
                                root = Tk()
                                root.title("Edit {}'s Inventory".format(self.charactername))

                                #auto fits frame to match widgets
                                root.geometry("")

                                #actual call to create the frame
                                app = Inventory(master=root, name=self.name)
                                app.mainloop()
                                root.destroy()
                                self.master.deiconify()
                                self.cleanup()
                        else: #if not in the database, we get the path to the 
                                path = os.getcwd()
                                pathtofile = path+"/inventory/"+self.name+"_Inventory"+".csv"
                                cur = self.conn.cursor()
                                file = open(pathtofile, "w")
                                file.close()
                                cur.execute('INSERT INTO inventory VALUES (?, ?)', (self.name, pathtofile))
                                self.conn.commit()
                                cur.close()
                                root = Tk()
                                root.title("Edit {}'s Inventory".format(self.charactername))

                                #Center frame to window.
                                root.geometry("")
                                app = Inventory(master=root, name=self.name)
                                app.mainloop()
                                root.destroy()
                                self.master.deiconify()
                                self.cleanup()
                else:
                       messagebox.showwarning("AHHH!", "You're a nameless Wizard! Return when you have a name and campaign") 
        #Transitions from the name home to Flash Card Game. 
        def createSpells(self):
                '''
                Creates the Flash Card Frame
                '''
                if(self.name != None and self.charactername != ""):
                        cur = self.conn.cursor()
                        cur.execute("SELECT * FROM spells WHERE name=?", (self.name,))
                        rows = cur.fetchall()
                        cur.close()

                        if rows:
                                # Hide this Frame
                                self.master.withdraw()
                                root = Tk()
                                root.title("{}'s Spell Book".format(self.charactername))

                                #Center frame to window.
                                root.geometry("")

                                app = Spells(master=root, name=self.name)
                                app.mainloop()
                                root.destroy()
                                self.master.deiconify()
                        else:
                                messagebox._show("Spell Book", "Enter the specifications! *read handbook for more information*")
                                spellsize_s = simpledialog.askstring("Number of Columns", "MAX: 10 MIN: 3")
                                names = []
                                ok = 0
                                if(self.intcheck(spellsize_s)):
                                        check = int(float(spellsize_s))
                                        if(check >= 3 and check <= 10):
                                                count = int(float(spellsize_s))
                                                for var in list(range(count)):
                                                        name = simpledialog.askstring("Name of Column","")
                                                        if(name != "" and name != None):
                                                                names.append(name)
                                                                ok += 1
                                                        else:
                                                                messagebox.showwarning("tsk tsk","Try an actual column name")
                                                if(ok == count):
                                                        path = os.getcwd()
                                                        pathtofile = path+"/spells/"+self.name+"_Spells"+".csv"
                                                        cur = self.conn.cursor()
                                                        active = 0
                                                        cur.execute('INSERT INTO spells VALUES (?, ?, ?, ?)', (self.name, count, pathtofile, active))
                                                        self.conn.commit()
                                                        cur.close()
                                                        file = open(pathtofile, "w")
                                                        with file:
                                                                writer = csv.writer(file)
                                                                writer.writerow(names)
                                                        self.master.withdraw()
                                                        root = Tk()
                                                        root.title("{}'s Spells".format(self.charactername))

                                                        #Center frame to window.
                                                        root.geometry("")
                                                        app = Spells(master=root, name=self.name)
                                                        app.mainloop()
                                                        root.destroy()
                                                        self.master.deiconify()
                                        else:
                                                messagebox.showwarning("tsk tsk", "Try a number in the range 3-10")
                                else:
                                        messagebox.showwarning("OOPS!", "That there is not an INTEGER!")
                else:
                        messagebox.showwarning("AHHH!", "You're a nameless Wizard! Return when you have a name and campaign")

        def createStats(self):
                '''
                Creates the Stats Frame
                Does input validation for the max size of stats
                Made it a constant size since number of stats are most often than not constant aspects
                of a game
                '''
                if(self.name != None and self.charactername != ""):
                        cur = self.conn.cursor()
                        cur.execute("SELECT * FROM stats WHERE name=?", (self.name,))
                        rows = cur.fetchall()
                        cur.close()

                        if rows:
                                self.master.withdraw()
                                root = Tk()
                                root.title("{}'s Stats Sheet".format(self.charactername))

                                root.geometry("")

                                app = Stats(master=root, name=self.name)
                                app.mainloop()
                                root.destroy()
                                self.master.deiconify()
                        else:
                                messagebox._show("Stats Sheet", "Enter the specifications! *read handbook for more information*")
                                spellsize_s = simpledialog.askstring("Unique Stats", "MAX: 100 MIN: 1")
                                statsvalues = []
                                ok = 0
                                oks = 0
                                active = 0
                                if(self.intcheck(spellsize_s)):
                                        check = int(float(spellsize_s))
                                        if(check >= 1 and check <= 100):
                                                count = int(float(spellsize_s)) #tracking the number of columns entered by the user if it meets the range requirements
                                                for var in list(range(count)):
                                                        name = simpledialog.askstring("Name","Enter Name of Stat")
                                                        name = str(var+1)+". "+name
                                                        if(name != "" and name != None):
                                                                ok += 1
                                                                value = simpledialog.askstring("Value", "Enter Value to be stored") #keeps track of an the value
                                                                if(value != "" and value != None):
                                                                        statsvalues.append([name,value])
                                                                        oks += 1
                                                                else:
                                                                        messagebox.showwarning("tsk tsk","Try an actual value")
                                                        else:
                                                                messagebox.showwarning("tsk tsk","Try an actual stats name")
                                                if(ok == count and oks == count): #makeing sure all stats have names and all are initialized to values
                                                        path = os.getcwd()
                                                        pathtofile = path+"/stats/"+self.name+"_Stats"+".csv"
                                                        cur = self.conn.cursor()
                                                        active = 0
                                                        cur.execute('INSERT INTO stats VALUES (?, ?, ?, ?)', (self.name, count, pathtofile, active)) #inserts the values into the database
                                                        self.conn.commit()
                                                        cur.close()
                                                        file = open(pathtofile, "w") #double checking to ensure the file is created or else errors happen
                                                        file.close()
                                                        file  = open(pathtofile, "a") #actually opens for writing
                                                        for var in list(range(count)):
                                                                writer = csv.writer(file)
                                                                writer.writerow(statsvalues[var])
                                                                file.flush() #doubly ensures file doesn't get overwritten and emptied
                                                        self.master.withdraw()
                                                        root = Tk()
                                                        root.title("{}'s Spells".format(self.charactername))

                                                        root.geometry("")
                                                        app = Stats(master=root, name=self.name)
                                                        app.mainloop()
                                                        root.destroy()
                                                        self.master.deiconify()
                else:
                       messagebox.showwarning("AHHH!", "You're a nameless Wizard! Return when you have a name and campaign") 
        
        """
        used throughout this file
        takes a string and returns whether or not
        it can be converted into an integer
        """
        def intcheck(self, check):
                try:
                        test = int(float(check))
                        if(isinstance(test, int)):
                                return True

                except:
                        return False

        """
        cleans the text widgets for inventory and biography
        then reads from the specified file to populate the fields
        this is necessary to ensure no repeat inputs
        """
        def cleanup(self):
            self.inv.delete('1.0', END)
            self.bio.delete('1.0', END)
            path = os.getcwd()+"/inventory/"+self.name+"_Inventory.csv"
            if os.path.exists(path):
                    with open(path) as f: 
                       reader = csv.reader(f)
                       for row in reader:
                          self.inv.insert(tk.END, row[0]+"\n")  
            biopath = os.getcwd()+"/bio/"+self.name+"_Bio.csv"
            if os.path.exists(biopath):
                with open(biopath) as f: 
                   reader = csv.reader(f)
                   for row in reader:
                      self.bio.insert(tk.END, row[0]+"\n")  