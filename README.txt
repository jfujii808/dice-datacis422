==MYTHICAL DICE SYSTEM 1.0 README===

Updated: 6/3/19



--PROGRAM AUTHORS--

Jonathan Fujii

Jeremy King

Jake Gianola

Cameron McKeown

Rico Williams



--BRIEF DESCRIPTION--

This program allows for a person to make

a customized die that emulates a physical die.

This die is saved, updated, and can be subsequently

rolled as per a normal die.



--CREATION DOCUMENTATION--

Mythical Dice was made over the month of May 2019.

It was created for the second project assignment of 
CIS422:
Software Methodologies.




--SETUP INSTRUCTIONS--

For application setup instructions, please see:

Installation_Instructions.txt




--USER INSTRUCTIONS--

For user documentation for application, please see:

User_Documentation.pdf




--DEVELOPER/PROGRAMMER INSTRUCTIONS--

For developer documentation, please see:

Programmer_Documentation.pdf




--SOFTWARE DEPENDENCIES--

Mythical Dice was designed for Python 3.7.1 and intended

to be used on Macintosh OSX 10.13 or 10.14

or Windows 10 OS


It requires the external Python library: Pygame



--SUBDIRECTORY INFORMATION--

src: Has all the dice roller python files and assets

toolkit: Has all toolkit python files and assets