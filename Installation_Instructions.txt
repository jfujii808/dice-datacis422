==MYTHICAL DICE 1.0 INSTALLATION INSTRUCTIONS==

Updated: 6/3/19



--PROGRAM AUTHORS--

Jonathan Fujii

Jeremy King

Jake Gianola

Cameron McKeown

Rico Williams



--STEP ONE--

Install python

https://www.python.org/downloads/



--STEP TWO--

Open terminal and enter the following:


$ pip3 install pygame



--STEP THREE--

Double click or execute either rundiceroller.sh or runtoolkit.sh



Mac/Linux Note:
    
The script will not work as described unless it has executable permissions
    
To give the script executable permissions:
    
1.  Navigate to the location of the scripts in the terminal
    
2.  Enter the command "chmod +x rundiceroller.sh runtoolkit.sh"
    
3.  Now you can run the scripts as described
    (or from the command line with ./rundiceroller.sh or ./runtoolkit.sh respectively)
