#Project 2 Mythical Dice 6/4/2019
#Component Authors: Jonathan Fujii, Jeremy King, Jake Gianola
from tkinter import *
from tkinter import ttk
import os

try:
    from dicemaker import *
    from diceobject import *
    from roller import RollWindow
except ImportError:
    from src.dicemaker import *
    from src.diceobject import *
    from src.roller import RollWindow

class DiceGallery(Frame):
    def __init__(self, master=None, roller=None):

        Frame.__init__(self, master)

        self.roller = RollWindow((480, 320))

        self.twoside = "Coin"
        #self.testval = dice_obj("TEST DICE", 20, "RED", "BLUE", "#0000CC", None, None, 0, [], 0, "diceassets/d20blue.png", None)
        
        # Pictures
        d2 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d2red.gif")
        d4 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d4orange.gif")
        d6 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d6yellow.gif")
        d8 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d8green.gif")
        d10 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d10blue.gif")
        d12 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d12purple.gif")
        d20 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d20pink.gif")
        d100 = os.path.join(os.path.dirname(os.path.abspath(__file__)), "diceassets/d100white.gif")
        self.image2 = PhotoImage(file = d2) #"diceassets/d2red.gif")
        self.image4 = PhotoImage(file = d4) #"diceassets/d4orange.gif")
        self.image6 = PhotoImage(file = d6) #"diceassets/d6yellow.gif")
        self.image8 = PhotoImage(file = d8) #"diceassets/d8green.gif")
        self.image10 = PhotoImage(file = d10) #"diceassets/d10blue.gif")
        self.image12 = PhotoImage(file = d12) #"diceassets/d12purple.gif")
        self.image20 = PhotoImage(file = d20) #"diceassets/d20pink.gif")
        self.image100 = PhotoImage(file = d100) #"diceassets/d100white.gif")
        
        self.D2dice = []
        self.D4dice = []
        self.D6dice = []
        self.D8dice = []
        self.D10dice = []
        self.D12dice = []
        self.D20dice = []
        self.D100dice = []
        
        self.dicelist = []
        self.dicedict = {}

        self.fileupdate()

            # self.roller = roller
            # self.testDice = dice_obj("test dice", "BLUE", "WHITE", "This is a test dice object", 20)
            
        self.createWidgets()
        master.protocol("WM_DELETE_WINDOW", self.quit)
        
        #This function updates the dicegallery after the dicemaker window closes
    def fileupdate(self):
        self.dicelist = []
        self.dicedict = {}
        self.D2dice = []
        self.D4dice = []
        self.D6dice = []
        self.D8dice = []
        self.D10dice = []
        self.D12dice = []
        self.D20dice = []
        self.D100dice = []
        
        rootpath = os.path.dirname(os.path.abspath(__file__)) #.../src/
        dicepath = os.path.join(rootpath, "diceobjects")
        filenames = []
        if os.path.isdir(dicepath):
            filenames = [name for name in os.listdir(dicepath) if not os.path.isdir(os.path.join(dicepath, name)) and len(name) > 4 and name[-4:] == ".txt"]
        
      

        for filename in filenames:
            file = open(os.path.join(rootpath, "diceobjects", filename), 'r')
            line = file.readline()
            dicevalues = line.split(";")
                            
            if len(dicevalues) < 12: # list too short
                continue
            #Order for dice object is: name, dtype, color, fontcolor, fontcolorhtml, weighted, favorednum, weight, log, lifetimerolls, average, image, notes
            Mydie = dice_obj(dicevalues[0], dicevalues[1], dicevalues[2], dicevalues[3], dicevalues[4], dicevalues[5], dicevalues[6], dicevalues[7], dicevalues[8], dicevalues[9], dicevalues[10], dicevalues[11], dicevalues[12])
                        
            self.dicelist.append(Mydie)
            self.dicedict[dicevalues[0]] = Mydie
                            
            if dicevalues[1] == "2":
                self.D2dice.append(dicevalues[0])
                            
            elif dicevalues[1] == "4":
                self.D4dice.append(dicevalues[0])
                        
            elif dicevalues[1] == "6":
                self.D6dice.append(dicevalues[0])
                            
            elif dicevalues[1] == "8":
                self.D8dice.append(dicevalues[0])
                        
            elif dicevalues[1] == "10":
                self.D10dice.append(dicevalues[0])
                        
            elif dicevalues[1] == "12":
                self.D12dice.append(dicevalues[0])
                
            elif dicevalues[1] == "20":
                self.D20dice.append(dicevalues[0])
                        
            else:
                self.D100dice.append(dicevalues[0])

    # self.roller = roller
    # self.testDice = dice_obj("test dice", "BLUE", "WHITE", "This is a test dice object", 20)
    
        self.createWidgets()
    
    #OPENS DICE ROLLER
    def open(self, diceObj):
        self.master.withdraw()
        self.roller.use(diceObj)
        while self.roller.active:
            self.roller.update()
        # window closes itself
        self.master.deiconify()
        self.fileupdate()

    def makedice(self, event=None):
        #self.master.withdraw()
        root = Tk()
        root.title("Dice Maker")
        w=500
        h=300
        ws = root.winfo_screenwidth() # width of the screen
        hs = root.winfo_screenheight()
        x = (ws/2) - (w/2)
        y = (hs/2) - (h/2)
        root.geometry("%dx%d+%d+%d" % (w, h, x, y))
        app = DiceMaker(master=root)
        app.mainloop()
        root.destroy()
        self.fileupdate()
        #self.master.deiconify()       

    def createWidgets(self):
        
        self.header = Label(root, text = "Dice Gallery", font = ("Courier",16))
        
        self.D2label = Label(root, text = "D2: ")
        self.D2pic = Label(root, image = self.image2)
        #self.D2dice = ["Please choose a die"]
        self.D2dropbox = ttk.Combobox(root,state= "readonly", values = self.D2dice)
        self.D2button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D2dropbox.get()]))
        
        self.D4label = Label(root, text = "D4: ")
        self.D4pic = Label(root, image = self.image4)
        #self.D4dice = ["Please choose a die"]
        self.D4dropbox = ttk.Combobox(root, state= "readonly",values = self.D4dice)
        self.D4button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D4dropbox.get()]))
        
        self.D6label = Label(root, text = "D6: ")
        self.D6pic = Label(root, image = self.image6)
        #self.D6dice = ["Please choose a die"]
        self.D6dropbox = ttk.Combobox(root,state= "readonly", values = self.D6dice)
        self.D6button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D6dropbox.get()]))
        
        self.D8label = Label(root, text = "D8: ")
        self.D8pic = Label(root, image = self.image8)
        #self.D8dice = ["Please choose a die"]
        self.D8dropbox = ttk.Combobox(root, state= "readonly",values = self.D8dice)
        self.D8button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D8dropbox.get()]))
        
        self.D10label = Label(root, text = "D10: ")
        self.D10pic = Label(root, image = self.image10)
        #self.D10dice = ["Please choose a die"]
        self.D10dropbox  = ttk.Combobox(root, state= "readonly",values = self.D10dice)
        self.D10button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D10dropbox.get()]))
        
        self.D12label = Label(root, text = "D12: ")
        self.D12pic = Label(root, image = self.image12)
        #self.D12dice = ["Please choose a die"]
        self.D12dropbox = ttk.Combobox(root, state= "readonly",values = self.D12dice)
        self.D12button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D12dropbox.get()]))
        
        self.D20label = Label(root, text = "D20: ")
        self.D20pic = Label(root, image = self.image20)
        # self.D20dice = ["Please choose a die"]
        self.D20dropbox = ttk.Combobox(root, state= "readonly",values = self.D20dice)
        self.D20button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D20dropbox.get()]))
        
        self.D100label = Label(root, text = "D100: ")
        self.D100pic = Label(root, image = self.image100)
        #self.D100dice = ["Please choose a die"]
        self.D100dropbox = ttk.Combobox(root, state= "readonly",values = self.D100dice)
        self.D100button = Button(root, text = "Use",command = lambda: self.open(self.dicedict[self.D100dropbox.get()]))
        
        self.Dicecreatebutton = Button(root, text = "Create new dice",command = lambda: self.makedice())
        
        
        #Grid Organization
        self.header.grid(row=0,columnspan=3)
        
        self.D2label.grid(row= 1, column = 0, padx = (75,0), pady =(25, 0))
        self.D2pic.grid(row=1,column = 0, padx = (0,75), pady= (25,0))
        self.D2dropbox.grid(row = 2, column = 0, padx = 50)
        self.D2button.grid(row = 3, column = 0, padx = 50, pady=(0,25))
        
        self.D4label.grid(row= 1, column = 1, padx = (75,0), pady =(25, 0))
        self.D4pic.grid(row=1,column = 1, padx = (0,75), pady= (25,0))
        self.D4dropbox.grid(row = 2, column = 1, padx = 50)
        self.D4button.grid(row = 3, column = 1, padx = 50, pady=(0,25))
        
        self.D6label.grid(row= 1, column = 2, padx = (75,0), pady =(25, 0))
        self.D6pic.grid(row=1,column = 2, padx = (0,75), pady= (25,0))
        self.D6dropbox.grid(row = 2, column = 2, padx = 50)
        self.D6button.grid(row= 3, column = 2, padx = 50, pady =(0, 25))
        
        self.D8label.grid(row= 4, column = 0, padx = (75,0), pady =(25, 0))
        self.D8pic.grid(row=4,column = 0, padx = (0,75), pady= (25,0))
        self.D8dropbox.grid(row= 5, column = 0, padx = 50)
        self.D8button.grid(row= 6, column = 0, padx = 50, pady =(0, 25))
        
        self.D10label.grid(row= 4, column = 2, padx = (75,0), pady =(25, 0))
        self.D10pic.grid(row=4,column = 2, padx = (0,75), pady= (25,0))
        self.D10dropbox.grid(row= 5, column = 2, padx = 50)
        self.D10button.grid(row= 6, column = 2, padx = 50, pady =(0, 25))
        
        self.D12label.grid(row= 7, column = 0, padx = (75,0), pady =(25, 0))
        self.D12pic.grid(row=7,column = 0, padx = (0,75), pady= (25,0))
        self.D12dropbox.grid(row= 8, column = 0, padx = 50)
        self.D12button.grid(row= 9, column = 0, padx = 50, pady =(0, 25))
        
        self.D20label.grid(row= 7, column = 1, padx = (75,0), pady =(25, 0))
        self.D20pic.grid(row=7,column = 1, padx = (0,75), pady= (25,0))
        self.D20dropbox.grid(row= 8, column = 1, padx = 50)
        self.D20button.grid(row= 9, column = 1, padx = 50, pady =(0, 25))
        
        self.D100label.grid(row= 7, column = 2, padx = (75,0), pady =(25, 0))
        self.D100pic.grid(row=7,column = 2, padx = (0,75), pady= (25,0))
        self.D100dropbox.grid(row= 8, column = 2, padx = 50)
        self.D100button.grid(row= 9, column = 2, padx = 50, pady = (0, 25))
        
        self.Dicecreatebutton.grid(row = 5, column = 1, padx = 50)
        
root = Tk()
DiceGallery(root)
root.mainloop()
root.destroy()
        
