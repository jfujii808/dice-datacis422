# CIS 422 Project 2 – Dice Roller
# Jeremy King
# 5/21/2019

# This program requires the pygame module.
# To install it run "pip3 install pygame" or "pip install pygame" from the command line
# ref: https://www.pygame.org/docs/

import pygame
import random as r
from math import *

try: # assume one directory up
	from src.components import *
	from src.diceobject import *
except ImportError: # assume same directory
	from components import * 
	from diceobject import *


class RollView:
	def __init__(self, window_size:tuple):
		self.ws = window_size
		self.dice = None # use self.load(diceObject)
		self.mpos = (0, 0)
		self.redraw = False # something needs redraw
		self.fulldraw = True # everything needs redraw
		self.close = False # flag to request window close

		self.font1 = pygame.font.Font(None, 24) # default font
		self.font2 = pygame.font.Font(None, 36) # larger font

		# calculations
		x = window_size[0]
		y = window_size[1]
		vert = x - x/5
		horz = y - y/4
		spc = (y-horz)/5
		col1w = 70
		btnh = 50
		col2w = 120
		col3p = spc*3+col1w+col2w

		# components
		self.rollButton = Button((spc*2+col1w, horz+spc), col2w, btnh, self.font2, "Roll", COLOR_PRIMARY)
		self.silentButton = Button((spc, horz+spc), col1w, btnh/2, self.font1, "Fidget", COLOR_SILENT)
		self.deleteButton = Button((spc, horz+spc+btnh/2), col1w, btnh/2, self.font1, "Delete", COLOR_DELETE)
		self.rollHistory = RollHistory((vert, 0), x-vert, y-spc, self.font1)
		self.advCheckBox = CheckBox((col3p, horz+spc), vert-spc-col3p, (y-horz)/2-spc, self.font1, "Advantage")
		self.disCheckBox = CheckBox((col3p, horz+(y-horz)/2), vert-spc-col3p, (y-horz)/2-spc, self.font1, "Disadvantage")
		self.diceDisplay = DiceDisplay((0, 0), vert, horz, self.font1, 50)
		self.vertLine = Line((vert, 0), (vert, y))
		self.horzLine = Line((0, horz), (vert, horz))
		self.components = [self.rollButton, self.silentButton, self.deleteButton, self.rollHistory, self.advCheckBox, self.disCheckBox, self.diceDisplay, self.vertLine, self.horzLine]

	def hover(self, pos):
		self.mpos = pos
		for component in self.components:
			if component.hover(pos):
				self.redraw = True
				break
	
	def click(self, pos):
		if self.diceDisplay.deleting:
			if self.deleteButton.click(pos):
				self.rollHistory.load(self.dice)
				self.diceDisplay.cancel_delete()
				self.deleteButton.set_text("Delete")
				self.redraw = True
			return # accept no other inputs while in delete animation

		for component in self.components:
			if component.click(pos):
				if component == self.advCheckBox and self.advCheckBox.checked:
					self.disCheckBox.uncheck()
				elif component == self.disCheckBox and self.disCheckBox.checked:
					self.advCheckBox.uncheck()
				elif component == self.rollButton:
					mode = 0
					if self.advCheckBox.checked:
						mode = 1
					if self.disCheckBox.checked:
						mode = 2
					self.diceDisplay.roll(mode)
				elif component == self.silentButton:
					mode = 0
					if self.advCheckBox.checked:
						mode = 1
					if self.disCheckBox.checked:
						mode = 2
					self.diceDisplay.roll(mode, silent=True)
				elif component == self.deleteButton:
					self.rollHistory.load(None)
					self.diceDisplay.delete()
					self.deleteButton.set_text("Cancel")

				self.redraw = True
				break

	def draw(self, screen) -> bool: # returns true if something was drawn
		if self.redraw: # something needs redraw
			for component in self.components:
				component.draw(screen)
			pygame.display.flip() # update display
			if self.diceDisplay.finished:
				self.diceDisplay.finished = False
				if self.diceDisplay.deleting == True: # deleting
					self.dice.delete()
					self.close = True
				self.rollHistory.redraw = True
				self.redraw = True
			else:
				self.redraw = self.diceDisplay.animating or self.diceDisplay.deleting # false when not animating
			return True

		elif self.fulldraw: # everything needs redraw
			screen.fill(COLOR_BLACK) # fill screen with black so we can re-draw from scratch
			for component in self.components:
				component.draw(screen, True)
			pygame.display.flip() # update display
			self.fulldraw = False
			return True
		
		return False

	def load(self, diceObject): # load dice into relevant components
		self.dice = diceObject
		self.rollHistory.load(diceObject)
		self.diceDisplay.load(diceObject)
		self.advCheckBox.uncheck()
		self.disCheckBox.uncheck()


class RollWindow:
	def __init__(self, window_size:tuple):
		self.ws = window_size
		self.active = False		
		self.view = None # initialized in _create()
		self.clock = pygame.time.Clock()

	def use(self, diceObject): # use dice in the window
		if not self._check_dice(diceObject):
			return
		if not self.active:
			self._create() # create pygame window
		self.view.load(diceObject) # load the dice into the view
	
	def close(self): # close window
		print("Closing Roller...")
		self.active = False
		self.view = None
		pygame.quit()
		# TODO cleanup

	def update(self): # process inputs and draw
		if not self.active: # no window
			return

		for event in pygame.event.get(): # process inputs
			if event.type == pygame.QUIT: # clicking X on window or ctrl+C in cmd will exit loop
				self.close()
				return
			if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
				self.view.click(pygame.mouse.get_pos()) # handle click
		self.view.hover(pygame.mouse.get_pos()) # tell view mouse position

		self.view.draw(self.screen) # draw
		if self.view.close:
			self.close()
		self.clock.tick(60)

	def _create(self): # create pygame window
		pygame.init() # initialize pygame
		pygame.display.set_caption("Dice Roller") # window title
		self.screen = pygame.display.set_mode(self.ws) # initialize game window
		self.view = RollView(self.ws) # view with draw methods
		self.active = True

	def _check_dice(self, diceObject) -> bool: # true if diceObject valid
		msg = "Error loading dice, "
		try:
			if not isinstance(diceObject.name, (str)):
				msg += "Invalid name: \""+str(diceObject.name)+"\" of type "+str(type(diceObject.name))
			elif not isinstance(diceObject.average, (int, float)):
				msg += "Invalid average: \""+str(diceObject.average)+"\" of type "+str(type(diceObject.average))
			elif not isinstance(diceObject.log, (list)):
				msg += "Invalid log: \""+str(diceObject.log)+"\" of type "+str(type(diceObject.log))
			elif not isinstance(diceObject.image, (str)):
				msg += "Invalid image: \""+str(diceObject.image)+"\" of type "+str(type(diceObject.image))
			elif not isinstance(diceObject.dtype, (int)):
				msg += "Invalid dtype: \""+str(diceObject.dtype)+"\" of type "+str(type(diceObject.dtype))
			elif not isinstance(diceObject.fontcolorhtml, (str)):
				msg += "Invalid color: \""+str(diceObject.fontcolorhtml)+"\" of type "+str(type(diceObject.fontcolorhtml))
			else:
				return True
		except:
			msg += "Missing field"
		print(msg)
		return False


if __name__ == "__main__":
	roller = RollWindow((480, 320))
	dice = dice_obj("Sample D20", 20, "BLUE", "RED", "#880000", "True", None, 0, "0", 0, 0, "diceassets/d20blue.png", None)
	roller.use(dice)
	while roller.active:
		roller.update()