#Project 2 Mythical Dice 6/4/2019
#Component Authors: Jonathan Fujii, Jeremy King, Jake Gianola
from tkinter import *
from tkinter import ttk
import pygame
import random
import os

class DiceMaker(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)
        self.pack()
        self.name = "MyDie"
        self.sides = "20" # Default sides is 20
        self.color = "RED" # Default color is red
        self.textcolor = "BLACK" # Default text is black
        self.htmltextcode = "#000000"
        self.weighted = "False" # Default status for dice is not weighted (False)
        self.favorednum = "10"
        self.weight = "2"
        self.log = "[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]"
        self.lifetimerolls = "0"
        self.average = "10"
        self.staticimage = "diceassets/d20red.png"
        self.notes = "Insert note"
        
        self.twoside = "Coin"
        self.testval = "Kaiowhat"
        
        self.htmldict = {"RED":"#FF0000", "ORANGE":"#FF8000", "YELLOW":"#FFFF00", "GREEN":"#00FF00",
                         "BLUE":"#0000FF", "PURPLE":"#8000FF", "PINK":"#FF00BF", "WHITE":"#FFFFFF",
                         "BROWN":"#8A2908", "GREY":"#848484", "BLACK":"#000000"}
        self.sidechoices = ["2","4","6","8","10","12","20","100"]
        self.colorchoices = ["RED","ORANGE","YELLOW","GREEN","BLUE","PURPLE","PINK","WHITE","BROWN","GREY","BLACK"]
        self.createWidgets()
        master.protocol("WM_DELETE_WINDOW", self.quit)
    
    #This is called when the dicemaker window is closed and it updates the Dicegallery accordingly
    def quit_and_update(self):
        self.master.update()
        self.quit()

    def writedice(self, event=None):
        # If the dice is weighted it generates the favored num & weight % here
        # This is the number that the die is weighted toward
        rannum = random.randint(0,int(self.sides))
        self.favorednum = str(rannum)
        # This is a percentage between 0.1% and 10%
        weightint = random.randint(0,100)
        weightlong = weightint / 10
        self.weight = str(weightlong)

        # This sets the die's assigned image
        lowercolor = str(self.color).lower()
        self.staticimage = "diceassets/d"+self.sides+lowercolor+".png"

        # Actually writes to the dice
        rootpath = os.path.dirname(os.path.abspath(__file__)) #.../src/
        filepath = os.path.join(rootpath, "diceobjects", self.name+".txt")
        
        try:
            dicefile = open(filepath, 'w+')
            dicefile.seek(0,2)
            dicefile.write(self.name+';')
            dicefile.write(self.sides+';')
            dicefile.write(self.color+';')
            dicefile.write(self.textcolor+';')
            dicefile.write(self.htmltextcode+';')
            dicefile.write(str(self.weighted)+';')
            dicefile.write(self.favorednum+';')
            dicefile.write(self.weight+';')
            dicefile.write(self.log+';')
            dicefile.write(self.lifetimerolls+';')
            dicefile.write(self.average+';')
            dicefile.write(self.staticimage+';')
            dicefile.write(self.notes+'\n')
        except:
            print("dicemaker... failure to write")
        finally:
            dicefile.close()

    def createWidgets(self):

        def setname():
            self.name = self.nameEntry.get()
            print(self.nameEntry.get())
            
        def setsides():
            if self.sidesOption.get() not in self.sidechoices:
                self.colorOption.delete(-1,50)
                self.sidesOption.insert(0,"INVALID NUMBER")
            else:
                self.sides = self.sidesOption.get()
                print(self.sides)
                
        def setcolor():
            # This variable capitalizes the entire string to fit format
            tempcolorcompare = str(self.colorOption.get()).upper()
            if tempcolorcompare not in self.colorchoices:
                self.colorOption.delete(-1,50)
                self.colorOption.insert(0,"INVALID COLOR")
            else:
                self.color = tempcolorcompare
                print(self.color)

        def settext():
            # This variable capitalizes the entire string to fit format
            tempcolorcompare = str(self.textOption.get()).upper()
            if tempcolorcompare not in self.colorchoices:
                self.textOption.delete(-1,50)
                self.textOption.insert(0,"INVALID COLOR")
            else:
                self.textcolor = tempcolorcompare
                self.htmltextcode = self.htmldict[tempcolorcompare]
                print(self.textcolor)
                print(self.htmltextcode)
                
        def setweight():
            # This variable makes the entry fit format
            weightcomparetemp = str(self.weightOption.get()).lower()
            weightcompare = weightcomparetemp.capitalize()
            if weightcompare != "True" and weightcompare != "False":
                self.weightOption.delete(-1,50)
                self.weightOption.insert(0,"INVALID WEIGHT")
            else:
                self.weighted = weightcompare
                print(self.weighted)

        def setnotes():
            self.notes = self.notesOption.get()
            print(self.notes)
        
        self.header = Label(self, text = "Dice Maker", font = ("Helvetica",16))
        self.nameLabel = Label(self, text = "Name:")
        self.nameEntry = Entry(self, bd = 5)
        self.nameButton = Button(self, text = "Confirm Name", command = lambda: setname())
        self.sidesLabel = Label(self, text = "Sides:")
        self.sidesOption = ttk.Combobox(self, state="readonly", values=self.sidechoices)
        self.sidesButton = Button(self, text = "Confirm Sides", command = lambda: setsides())
        self.colorLabel = Label(self, text = "Die Color:")
        self.colorOption = ttk.Combobox(self, state="readonly", values=self.colorchoices)
        self.colorButton = Button(self, text = "Confirm Die Color", command = lambda: setcolor())
        self.textLabel = Label(self, text = "Text Color:")
        self.textOption =ttk.Combobox(self, state="readonly", values=self.colorchoices)
        self.textButton = Button(self, text = "Confirm Text Color", command = lambda: settext())
        self.weightLabel = Label(self, text = "Weighted (True/False):")
        self.weightOption = ttk.Combobox(self, state="readonly", values=["True", "False"])
        self.weightButton = Button(self, text = "Confirm Weighted", command = lambda: setweight())
        self.notesLabel = Label(self, text = "Notes:")
        self.notesOption = Entry(self, bd = 5)
        self.notesButton = Button(self, text = "Confirm Notes", command = lambda: setnotes())
        self.makebutton = Button(self, text = "Make a Die", command = lambda: self.writedice())

        self.header.grid(row=0, columnspan=3, sticky=W+E)
        self.nameLabel.grid(row=1, column=0, sticky=W+E)
        self.nameEntry.grid(row=1, column=1, sticky=W+E)
        self.nameButton.grid(row=1, column=2, sticky=W+E)
        self.sidesLabel.grid(row=2, column=0, sticky=W+E)
        self.sidesOption.grid(row=2, column=1, sticky=W+E)
        self.sidesButton.grid(row=2, column=2, sticky=W+E)
        self.colorLabel.grid(row=3, column=0, sticky=W+E)
        self.colorOption.grid(row=3, column=1, sticky=W+E)
        self.colorButton.grid(row=3, column=2, sticky=W+E)
        self.textLabel.grid(row=4, column=0, sticky=W+E)
        self.textOption.grid(row=4, column=1, sticky=W+E)
        self.textButton.grid(row=4, column=2, sticky=W+E)
        self.weightLabel.grid(row=5, column=0, sticky=W+E)
        self.weightOption.grid(row=5, column=1, sticky=W+E)
        self.weightButton.grid(row=5, column=2, sticky=W+E)
        self.notesLabel.grid(row=6, column=0, sticky=W+E)
        self.notesOption.grid(row=6, column=1, sticky=W+E)
        self.notesButton.grid(row=6, column=2, sticky=W+E)
        self.makebutton.grid(row=12, columnspan=3, sticky=W+E)


        
